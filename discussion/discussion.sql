-- ALTER TABLE albums RENAME COLUMN data_released TO date_released;


	-- TO CHANGE THE DATA TYPE OF COLUMN
-- ALTER TABLE songs MODIFY COLUMN song_genre VARCHAR(50);


-- [SECTION] INSERT records (CREATE);

-- INSERT INTO name_of_table (columns_on_table) VALUES (values_per_column); 

INSERT INTO artists(name) VALUES ('Rivermaya');
INSERT INTO artists(name) VALUES ('Psy');

INSERT INTO albums(album_title, date_released, artist_id) VALUES ('PSY 6', "2012-1-1", 2);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Trip', '1996-1-1', 1);

INSERT INTO songs(song_title, song_length, song_genre, album_id) VALUES ('Gangnam Style', 253, 'K-pop', 1);
INSERT INTO songs(song_title, song_length, song_genre, album_id) VALUES ('Kundiman', 234, 'OPM', 2);
INSERT INTO songs(song_title, song_length, song_genre, album_id) VALUES ('Kisapmata', 259, "OPM", 2);

-- [SECTION] SELECTING records (RETRIVE);
-- Retrieve all the records in a specific table:
	-- SELECT * FROM table_name;
SELECT * FROM songs;

-- Retrieve all the records but not all the columns:
SELECT song_title, song_length FROM songs;

-- Retrieve record given a specific condition:
SELECT * FROM songs WHERE song_genre = "OPM";

SELECT song_title, song_length FROM songs WHERE song_genre = "OPM";

-- AND and OR key word
SELECT * FROM songs WHERE song_length > 240 AND song_genre = "OPM";
SELECT * FROM songs WHERE song_length > 240 OR song_genre = "OPM";

-- [SECTION] UPDATING records;
-- UPDATE table_name SET column_name = value WHERE condition;
UPDATE songs SET song_length = 240;
UPDATE songs SET song_length = 259 WHERE id = 2;

--UPDATE more than 1
UPDATE songs SET song_length = 300 WHERE song_length > 240 AND song_genre = 'OPM';

-- [SECTION] DELETING records;
-- DELETE FROM table_name WHERE condition;
DELETE FROM songs WHERE song_genre = "OPM" AND song_length > 240;